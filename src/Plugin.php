<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerGitHooksDeployedPackage;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Interactiv4\ComposerInstallerDeployedPackage\PackageTypesProvider;

/**
 * Class Plugin.
 *
 * @api
 */
class Plugin implements PluginInterface, EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public function activate(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deactivate(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function uninstall(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PackageEvents::PRE_PACKAGE_UPDATE => [
                ['registerPackageTypeWithPackageEvent', 10],
            ],
            PackageEvents::PRE_PACKAGE_INSTALL => [
                ['registerPackageTypeWithPackageEvent', 10],
            ],
            ScriptEvents::PRE_INSTALL_CMD => [
                ['registerPackageTypeWithScriptEvent', 10],
            ],
            ScriptEvents::PRE_UPDATE_CMD => [
                ['registerPackageTypeWithScriptEvent', 10],
            ],
            // Ensure execute fix permissions after other installers with priority 0
            ScriptEvents::POST_INSTALL_CMD => [
                ['makeGitHooksExecutable', -10],
            ],
            ScriptEvents::POST_UPDATE_CMD => [
                ['makeGitHooksExecutable', -10],
            ],
        ];
    }

    /**
     * @param PackageEvent $event
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function registerPackageTypeWithPackageEvent(PackageEvent $event): void
    {
        $this->registerPackageType();
    }

    /**
     * @param Event $event
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function registerPackageTypeWithScriptEvent(Event $event): void
    {
        $this->registerPackageType();
    }

    /**
     * @param Event $event
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function makeGitHooksExecutable(Event $event): void
    {
        if (\is_dir('./.git')) {
            \exec('chmod -R +x ./.git/hooks/');
        }
    }

    /**
     * Register package type.
     */
    private function registerPackageType(): void
    {
        if (\is_dir('./.git')) {
            PackageTypesProvider::getInstance()->addPackageType('git-hooks-deployed-package', './.git/hooks');
        }
    }
}
